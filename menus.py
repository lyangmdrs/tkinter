from Tkinter import *

def newfile():
	label1 = Label(app,text='You did clicked on New in File menu').pack()

# basics

app = Tk()
app.title('Menu test')
app.geometry('250x250+600+200')

label1 = Label(app,text='An app with a menu bar').pack()

# Menu Construction

menubar=Menu(app)

# file menu

filemenu = Menu(menubar,tearoff=0)
filemenu.add_command(label='New', command=newfile)
filemenu.add_command(label='Open')
filemenu.add_command(label='Save')
menubar.add_cascade(label='File',menu=filemenu)

# help menu

helpmenu = Menu(menubar,tearoff=0)
helpmenu.add_command(label='Help Docs')
helpmenu.add_command(label='About')
menubar.add_cascade(label='Help',menu=helpmenu)

app.config(menu=menubar)
app.mainloop()
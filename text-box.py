from Tkinter import *

def func():
	mtext = ment.get()
	label2 = Label(mgui, text=mtext).pack()
	return

mgui = Tk()
ment = StringVar()

mgui.geometry('450x450+400+200')
mgui.title('Text Box Test')

label1 = Label(text='A GUI with a text box').pack()
button1 = Button(mgui,text='enter',command=func).pack()
textbox1 = Entry(mgui, textvariable=ment).pack()

mgui.mainloop()
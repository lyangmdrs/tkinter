import sys
from Tkinter import *
import tkMessageBox

def newfile():

	pass

def mAbout():
	
	tkMessageBox.showinfo(title='About',message='This is an About box!')
	
	return

def alert():
	
	tkMessageBox.showwarning(title = 'Test', message = 'This is an Alert!')

	return

def quit():
	
	_quit = tkMessageBox.askyesno(title = 'Quit', message = 'Do you really wan to quit?')

	if _quit == True:
		mygui.destroy()

	return


mygui = Tk()
mygui.geometry('400x350+600+200')
mygui.title('Message Box Test')


label1 = Label(mygui,text='This is an app whith a menu and a message box!').place(x=0,y=13)
button1 = Button(mygui, text = 'Test', command = alert).place(x=265,y=10)


# Menu Construction

menubar=Menu(mygui)

# file menu

filemenu = Menu(menubar,tearoff=0)
filemenu.add_command(label = 'New', command = newfile)
filemenu.add_command(label = 'Open')
filemenu.add_command(label = 'Save')
filemenu.add_command(label = 'Close', command = quit)
menubar.add_cascade(label='File',menu=filemenu)

# help menu

helpmenu = Menu(menubar,tearoff=0)
helpmenu.add_command(label='Help Docs')
helpmenu.add_command(label='About', command=mAbout)
menubar.add_cascade(label='Help',menu=helpmenu)

mygui.config(menu=menubar)
mygui.mainloop()
from Tkinter import *

mGui = Tk()

mGui.geometry('450x350+350+200')
mGui.title('My Window')

#label1 = Label(text='this is a packed label', fg='blue', bg='red').pack()
label2 = Label(text='this is a placed label', fg='magenta', bg='black').place(x = 100, y = 100)
label3 = Label(text='this is a label on a grid', fg='red', bg='gray').grid(row = 1, column = 1)
label4 = Label(text='this is another label bigger the others on a grid', fg='green', bg='yellow').grid(row = 2, column = 0)
label3 = Label(text='this is another label on a grid East aligned', fg='black', bg='white').grid(row = 3, column = 0, sticky=E)

mGui.mainloop()